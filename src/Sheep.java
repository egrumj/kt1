

public class Sheep {

   enum Animal {sheep, goat};

   //Sain koodi ülesehituse lingilt: https://stackoverflow.com/a/25855188.
   
   public static void main (String[] param) {
      // for debugging
   }
   
   public static void reorder (Animal[] animals) {
      int start = 0;
      int end = animals.length - 1;

      while(start < end){
         if(animals[start].equals(Animal.sheep)){
            if(animals[end].equals(Animal.goat)){
               Animal sheep = animals[start];
               animals[start] = animals[end];
               animals[end] = sheep;
               end--;
               start++;
            }else{
               end--;
            }
         }else{
            start++;
         }
      }
   }
}

